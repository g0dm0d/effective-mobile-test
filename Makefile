include .env

.PHONY: install run migration_up migration_down docker_compose_up docker_compose_down docker_clean

migration_up:
	migrate -path ./internal/store/postgres/migrations/ -database "$(POSTGRES_DSN)" -verbose up

migration_down:
	migrate -path ./internal/store/postgres/migrations/ -database "$(POSTGRES_DSN)" -verbose down

setup:
	cp ./configs/.env.example .env
	go mod tody

docker_build:
	docker build -f ./deployments/Dockerfile -t car-service:dev .

docker_compose_up:
	docker compose up

docker_compose_down:
	docker compose down
	docker rmi effectivemobile-service-api

docker_clean:
	docker stop car-service
	docker rm car-service
	docker rmi car-service:dev

run:
	go run cmd/app/main.go
