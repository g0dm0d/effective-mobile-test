package postgres

import (
	"context"
	"database/sql"

	"gitlab.com/g0dm0d/effective/internal/models"
	"gitlab.com/g0dm0d/effective/internal/store"
)

type PeopleStore struct {
	db *sql.DB
}

func NewPeopleStore(db *sql.DB) store.PeopleStore {
	return &PeopleStore{
		db: db,
	}
}

func (s *PeopleStore) Save(ctx context.Context, name, surname, patronymic string) (models.Person, error) {
	query := `INSERT INTO people (
		name,
		surname,
		patronymic
	)
	VALUES ($1, $2, $3)
	RETURNING id`

	owner := models.Person{
		Name:       name,
		Surname:    surname,
		Patronymic: patronymic,
	}

	err := s.db.QueryRowContext(ctx, query, name, surname, patronymic).Scan(
		&owner.ID,
	)
	if err != nil {
		return models.Person{}, err
	}

	return owner, nil
}

func (s *PeopleStore) Update(ctx context.Context, name, surname, patronymic string, ownerID int) (models.Person, error) {
	query := `UPDATE cars SET 
		name = $1,
		surname = $2,
		patronymic = $3
		WHERE id = $4
	RETURNING id, name, surname, patronymic`

	var person models.Person
	err := s.db.QueryRowContext(ctx, query, name, surname, patronymic, ownerID).Scan(
		&person.ID,
		&person.Name,
		&person.Surname,
		&person.Patronymic,
	)

	if err != nil {
		return models.Person{}, err
	}

	return person, nil
}

func (s *PeopleStore) Search(ctx context.Context, name, surname, patronymic string) (models.Person, error) {
	query := `SELECT 
		id,
		name,
		surname,
		patronymic
	FROM people
	WHERE name = $1
	AND surname = $2
	AND patronymic = $3`

	var owner models.Person
	err := s.db.QueryRowContext(ctx, query, name, surname, patronymic).Scan(
		&owner.ID,
		&owner.Name,
		&owner.Surname,
		&owner.Patronymic,
	)
	if err != nil {
		return models.Person{}, err
	}

	return owner, nil
}

func (s *PeopleStore) GetByID(ctx context.Context, ownerID int) (models.Person, error) {
	query := `SELECT 
		id,
		name,
		surname,
		patronymic
	FROM people
	WHERE id = $1`

	var owner models.Person
	err := s.db.QueryRowContext(ctx, query, ownerID).Scan(
		&owner.ID,
		&owner.Name,
		&owner.Surname,
		&owner.Patronymic,
	)
	if err != nil {
		return models.Person{}, err
	}

	return owner, nil
}
