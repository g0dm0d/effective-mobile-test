CREATE TABLE IF NOT EXISTS people (
  id          SERIAL      PRIMARY KEY,
  name        VARCHAR(40) NOT NULL,
  surname     VARCHAR(40) NOT NULL,
  patronymic  VARCHAR(40) NOT NULL
);

CREATE TABLE IF NOT EXISTS cars (
  id          SERIAL      PRIMARY KEY,
  reg_num     VARCHAR(40) NOT NULL UNIQUE,
  mark        VARCHAR(40) NOT NULL,
  model       VARCHAR(40) NOT NULL,
  year        SMALLINT    NOT NULL,
  owner_id    INT         REFERENCES people(id)
);

INSERT INTO public.people ("name", surname, patronymic) VALUES('Mia', 'Martinez', 'Rodriguez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Michael', 'Brown', 'Jones');
INSERT INTO public.people ("name", surname, patronymic) VALUES('John', 'Williams', 'Miller');
INSERT INTO public.people ("name", surname, patronymic) VALUES('John', 'Rodriguez', 'Martinez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Olivia', 'Jones', 'Rodriguez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('James', 'Williams', 'Rodriguez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Emma', 'Jones', 'Martinez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Olivia', 'Davis', 'Davis');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Isabella', 'Smith', 'Rodriguez');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Michael', 'Rodriguez', 'Williams');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Mia', 'Miller', 'Williams');
INSERT INTO public.people ("name", surname, patronymic) VALUES('Isabella', 'Johnson', 'Garcia');
INSERT INTO public.people ("name", surname, patronymic) VALUES('William', 'Jones', 'Rodriguez');


INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X123XX150', 'Toyota', 'Focus', 2012, 1);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X124XX150', 'Chevrolet', 'E-Class', 2019, 2);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X125XX150', 'Volvo', 'Focus', 2018, 3);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X126XX150', 'BMW', 'Vesta', 2023, 4);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X127XX150', 'Volkswagen', 'Focus', 2013, 5);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X128XX150', 'Honda', 'S60', 2009, 6);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X129XX150', 'Volvo', 'Camry', 2022, 7);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X130XX150', 'Honda', 'Vesta', 2009, 8);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X131XX150', 'Audi', 'Vesta', 2002, 9);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X132XX150', 'Honda', 'A4', 2021, 10);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X133XX150', 'Volvo', 'Vesta', 2000, 11);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X134XX150', 'Honda', 'Vesta', 2000, 12);
INSERT INTO public.cars (reg_num, mark, model, "year", owner_id) VALUES('X135XX150', 'Toyota', 'Focus', 2008, 13);
