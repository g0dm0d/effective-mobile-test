package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/g0dm0d/effective/internal/models"
	"gitlab.com/g0dm0d/effective/internal/server/schemas"
	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
	"gitlab.com/g0dm0d/effective/internal/store"
)

type CarStore struct {
	db *sql.DB
}

func NewCarStore(db *sql.DB) store.CarStore {
	return &CarStore{
		db: db,
	}
}

func (s *CarStore) Save(ctx context.Context, regNum string, mark, model string, year uint16, ownerID int) (models.Car, error) {
	query := `INSERT INTO cars (
		reg_num,
		mark,
		model,
		year,
		owner_id
	)
	VALUES ($1, $2, $3, $4, $5)
	RETURNING id, reg_num, mark, model, year, owner_id`

	var car models.Car
	err := s.db.QueryRowContext(ctx, query, regNum, mark, model, year, ownerID).Scan(
		&car.ID,
		&car.RegNum,
		&car.Mark,
		&car.Model,
		&car.Year,
		&car.OwnderID,
	)
	if err != nil {
		return models.Car{}, err
	}

	return car, nil
}

func (s *CarStore) Update(ctx context.Context, carID int, mark, model string, year uint16) (models.Car, error) {
	query := `UPDATE cars SET 
		mark = $1,
		model = $2,
		year = $3
		WHERE id = $4
	RETURNING id, reg_num, mark, model, year, owner_id`

	var car models.Car
	err := s.db.QueryRowContext(ctx, query, mark, model, year, carID).Scan(
		&car.ID,
		&car.RegNum,
		&car.Mark,
		&car.Model,
		&car.Year,
		&car.OwnderID,
	)

	if err != nil {
		return models.Car{}, err
	}

	return car, nil
}

func (s *CarStore) Search(ctx context.Context, params schemas.SearchParam) ([]dto.Car, error) {
	query := `
	SELECT 
		cars.id AS car_id,
		cars.reg_num,
		cars.mark,
		cars.model,
		cars.year,
		people.id AS owner_id,
		people.name AS owner_name,
		people.surname AS owner_surname,
		people.patronymic AS owner_patronymic
	FROM cars
	JOIN people ON cars.owner_id = people.id`

	conditions := []string{}

	if params.CarID != nil {
		conditions = append(conditions, fmt.Sprintf("cars.id = '%s'", params.CarID))
	}
	if params.RegNum != nil {
		conditions = append(conditions, fmt.Sprintf("cars.reg_num = '%s'", params.RegNum))
	}
	if params.Mark != nil {
		conditions = append(conditions, fmt.Sprintf("cars.mark = '%s'", params.Mark))
	}
	if params.Model != nil {
		conditions = append(conditions, fmt.Sprintf("cars.model = '%s'", params.Model))
	}
	if params.Year != nil {
		conditions = append(conditions, fmt.Sprintf("cars.year = '%s'", params.Year))
	}
	if params.OwnerID != nil {
		conditions = append(conditions, fmt.Sprintf("people.id = '%s'", params.OwnerID))
	}
	if params.OwnerName != nil {
		conditions = append(conditions, fmt.Sprintf("people.name = '%s'", params.OwnerName))
	}
	if params.OwnerSurname != nil {
		conditions = append(conditions, fmt.Sprintf("people.surname = '%s'", params.OwnerSurname))
	}
	if params.OwnerPatronymic != nil {
		conditions = append(conditions, fmt.Sprintf("people.patronymic = '%s'", params.OwnerPatronymic))
	}

	if len(conditions) > 0 {
		query += " WHERE " + strings.Join(conditions, " AND ")
	}

	query += " ORDER BY cars.id DESC LIMIT $1 OFFSET $2"

	cars := []dto.Car{}

	rows, err := s.db.QueryContext(ctx, query, params.PageSize, params.GetPageOffset())
	if err != nil {
		return []dto.Car{}, err
	}

	for rows.Next() {
		var car dto.Car

		rows.Scan(
			&car.ID,
			&car.RegNum,
			&car.Mark,
			&car.Model,
			&car.Year,
			&car.Owner.ID,
			&car.Owner.Name,
			&car.Owner.Surname,
			&car.Owner.Patronymic,
		)

		cars = append(cars, car)
	}

	return cars, nil
}
