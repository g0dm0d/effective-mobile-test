package store

import (
	"context"

	"gitlab.com/g0dm0d/effective/internal/models"
	"gitlab.com/g0dm0d/effective/internal/server/schemas"
	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
)

type CarStore interface {
	Save(ctx context.Context, regNum string, mark, model string, year uint16, ownerID int) (models.Car, error)
	Search(ctx context.Context, params schemas.SearchParam) ([]dto.Car, error)
	Update(ctx context.Context, carID int, mark, model string, year uint16) (models.Car, error)
}

type PeopleStore interface {
	Save(ctx context.Context, name, surname, patronymic string) (models.Person, error)
	Search(ctx context.Context, name, surname, patronymic string) (models.Person, error)
	Update(ctx context.Context, name, surname, patronymic string, ownerID int) (models.Person, error)
	GetByID(ctx context.Context, ownerID int) (models.Person, error)
}
