package car

import (
	"context"

	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
)

type SaveCarParams struct {
	RegNum string
}

func (s *Service) Save(ctx context.Context, params SaveCarParams) (dto.Car, error) {
	car, err := s.carApi.Find(ctx, params.RegNum)
	if err != nil {
		return dto.Car{}, err
	}

	person, err := s.peopleStore.Search(ctx, car.Owner.Name, car.Owner.Surname, car.Owner.Patronymic)
	if err != nil {
		person, err = s.peopleStore.Save(ctx, car.Owner.Name, car.Owner.Surname, car.Owner.Patronymic)
		if err != nil {
			return dto.Car{}, err
		}
	}

	cretedCar, err := s.carStore.Save(ctx, car.RegNum, car.Mark, car.Model, car.Year, person.ID)
	if err != nil {
		return dto.Car{}, err
	}
	return dto.Car{
		ID:     cretedCar.ID,
		RegNum: params.RegNum,
		Mark:   cretedCar.Mark,
		Model:  cretedCar.Model,
		Year:   cretedCar.Year,
		Owner: dto.Owner{
			ID:         person.ID,
			Name:       person.Name,
			Surname:    person.Surname,
			Patronymic: person.Patronymic,
		},
	}, nil
}
