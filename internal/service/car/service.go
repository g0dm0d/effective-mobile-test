package car

import (
	"context"

	"gitlab.com/g0dm0d/effective/internal/gateway/carapi"
	"gitlab.com/g0dm0d/effective/internal/server/schemas"
	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
	"gitlab.com/g0dm0d/effective/internal/store"
)

type Car interface {
	Save(ctx context.Context, params SaveCarParams) (dto.Car, error)
	Search(ctx context.Context, params schemas.SearchParam) ([]dto.Car, error)
	Update(ctx context.Context, params UpdateCarParams) (dto.Car, error)
}

type Service struct {
	carStore    store.CarStore
	peopleStore store.PeopleStore
	carApi      *carapi.Client
}

func NewService(carStore store.CarStore, peopleStore store.PeopleStore, carApi *carapi.Client) *Service {
	return &Service{
		carStore:    carStore,
		peopleStore: peopleStore,
		carApi:      carApi,
	}
}
