package car

import (
	"context"

	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
)

type UpdateCarParams struct {
	CarID int
	Mark  string
	Model string
	Year  uint16
}

func (s *Service) Update(ctx context.Context, params UpdateCarParams) (dto.Car, error) {
	car, err := s.carStore.Update(ctx, params.CarID, params.Mark, params.Model, params.Year)
	if err != nil {
		return dto.Car{}, err
	}

	owner, err := s.peopleStore.GetByID(ctx, car.OwnderID)
	if err != nil {
		return dto.Car{}, err
	}

	return dto.Car{
		ID:     car.ID,
		RegNum: car.RegNum,
		Mark:   car.Mark,
		Model:  car.Model,
		Year:   car.Year,
		Owner:  dto.Owner(owner),
	}, nil
}
