package car

import (
	"context"

	"gitlab.com/g0dm0d/effective/internal/server/schemas"
	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
)

func (s *Service) Search(ctx context.Context, params schemas.SearchParam) ([]dto.Car, error) {
	return s.carStore.Search(ctx, params)
}
