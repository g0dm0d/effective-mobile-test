package carapi

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
)

type Client struct {
	client *http.Client
}

func NewClient() *Client {
	return &Client{
		client: &http.Client{},
	}
}

func (c *Client) do(ctx context.Context, method string, endpoint string, payload interface{}, response interface{}) error {
	buffer := new(bytes.Buffer)
	if payload != nil {
		json.NewEncoder(buffer).Encode(payload)
	}

	req, err := http.NewRequestWithContext(ctx, method, endpoint, buffer)
	if err != nil {
		return err
	}

	if payload != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}

	return nil
}
