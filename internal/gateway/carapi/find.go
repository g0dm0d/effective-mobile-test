package carapi

import (
	"context"
	"net/http"
)

func (c *Client) Find(ctx context.Context, regNum string) (CarInfo, error) {
	var car CarInfo

	err := c.do(ctx, http.MethodGet, EndpointFind(regNum), nil, &car)
	if err != nil {
		return CarInfo{}, err
	}

	return car, nil
}
