package server

import (
	"fmt"
	"log/slog"
	"net"
	"net/http"

	"gitlab.com/g0dm0d/effective/internal/server/handler"
)

type Server struct {
	l      net.Listener
	server *http.Server
	logger *slog.Logger
}

func NewServer(addr string, port string, h handler.Handlers, logger *slog.Logger) (*Server, error) {
	l, err := net.Listen("tcp", fmt.Sprintf("%s:%s", addr, port))
	if err != nil {
		return nil, err
	}

	return &Server{
		l: l,
		server: &http.Server{
			Handler: newRouter(h, logger),
		},
		logger: logger,
	}, nil
}

func (s *Server) RunServer() error {
	s.logger.Info("Starting http server", "addr", s.l.Addr().String())
	return s.server.Serve(s.l)
}
