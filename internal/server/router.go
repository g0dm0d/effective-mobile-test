package server

import (
	"log/slog"
	"net/http"

	"gitlab.com/g0dm0d/effective/internal/server/handler"
	"gitlab.com/g0dm0d/effective/internal/server/middleware"
	"gitlab.com/g0dm0d/effective/internal/server/req"
)

func newRouter(h handler.Handlers, logger *slog.Logger) *http.ServeMux {
	r := http.NewServeMux()

	r.Handle("POST /car/save", middleware.CORS(req.NewHandler(h.Car.SaveCar, logger)))
	r.Handle("GET /car/search", middleware.CORS(req.NewHandler(h.Car.Search, logger)))
	r.Handle("PUT /car/{id}", middleware.CORS(req.NewHandler(h.Car.Update, logger)))

	return r
}
