package req

import (
	"errors"
	"log/slog"
	"net/http"

	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
)

type HandlerFunc func(*Ctx) error

type Handler struct {
	handlerFunc HandlerFunc
	logger      *slog.Logger
}

func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := &Ctx{
		Writer:  w,
		Request: r,
	}
	if err := h.handlerFunc(ctx); err != nil {
		var dtoErr dto.Error
		if !errors.As(err, &dtoErr) {
			dtoErr = dto.NewError("INTERNAL_SERVER_ERROR", http.StatusInternalServerError, "Internal server error", err)
		}
		err = ctx.WriteJSON(dtoErr, dtoErr.HttpCode)
		if err != nil {
			h.logger.Error("Error writing response", "error", err)
		}

		h.logger.Error("Handler error", "error", dtoErr)

	}
}
func NewHandler(f HandlerFunc, l *slog.Logger) Handler {
	return Handler{
		handlerFunc: f,
		logger:      l,
	}
}
