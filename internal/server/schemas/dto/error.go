package dto

import "fmt"

type Error struct {
	Code        string `json:"code"`
	HttpCode    int    `json:"http_code"`
	Description string `json:"description"`
	Inner       error  `json:"-"`
}

func (e Error) Error() string {
	return fmt.Sprintf("code: %s, http_code: %d, description: %s, inner: %v", e.Code, e.HttpCode, e.Description, e.Inner)
}

func NewError(code string, httpCode int, description string, inner error) Error {
	return Error{
		Code:        code,
		HttpCode:    httpCode,
		Description: description,
		Inner:       inner,
	}
}
