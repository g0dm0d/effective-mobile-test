package schemas

type UpdateCar struct {
	Mark  string `json:"mark"`
	Model string `json:"model"`
	Year  uint16 `json:"year"`
}
