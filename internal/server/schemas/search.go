package schemas

type SearchParam struct {
	Page            int
	PageSize        int
	CarID           interface{} `param:"car_id"`
	RegNum          interface{} `param:"reg_num"`
	Mark            interface{} `param:"mark"`
	Model           interface{} `param:"model"`
	Year            interface{} `param:"year"`
	OwnerID         interface{} `param:"owner_id"`
	OwnerName       interface{} `param:"owner_name"`
	OwnerSurname    interface{} `param:"owner_surname"`
	OwnerPatronymic interface{} `param:"owner_patronymic"`
}

func (s *SearchParam) GetPageOffset() int {
	return (s.Page - 1) * s.PageSize
}
