package handler

import (
	"net/http"
	"strconv"

	"gitlab.com/g0dm0d/effective/internal/pkg/filter"
	"gitlab.com/g0dm0d/effective/internal/server/req"
	"gitlab.com/g0dm0d/effective/internal/server/schemas"
	"gitlab.com/g0dm0d/effective/internal/server/schemas/dto"
	"gitlab.com/g0dm0d/effective/internal/service/car"
)

type Car struct {
	car car.Car
}

func NewHandlerCar(car car.Car) *Car {
	return &Car{
		car: car,
	}
}

func (c *Car) SaveCar(ctx *req.Ctx) error {
	var req schemas.AddCarsRequest

	err := ctx.ParseJSON(&req)
	if err != nil {
		return err
	}

	var cars []dto.Car
	for _, regNum := range req.RegNums {
		car, err := c.car.Save(ctx.Context(), car.SaveCarParams{RegNum: regNum})
		cars = append(cars, car)
		if err != nil {
			return err
		}
	}
	return ctx.WriteJSON(cars, http.StatusCreated)
}

func (c *Car) Search(ctx *req.Ctx) error {
	var params schemas.SearchParam
	queryParams := ctx.Request.URL.Query()

	pageStr := queryParams.Get("page")
	pageSizeSrt := queryParams.Get("size")

	page, err := strconv.Atoi(pageStr)
	if err != nil {
		return dto.NewError("INVALID_URL_PARAM", http.StatusBadRequest, "invalid page", err)
	}

	pageSize, err := strconv.Atoi(pageSizeSrt)
	if err != nil {
		return dto.NewError("INVALID_URL_PARAM", http.StatusBadRequest, "invalid page size", err)
	}

	filter.ParseQueryParams(queryParams, &params)
	params.Page = page
	params.PageSize = pageSize

	cars, err := c.car.Search(ctx.Context(), params)
	if err != nil {
		return err
	}

	return ctx.WriteJSON(cars, http.StatusOK)
}

func (c *Car) Update(ctx *req.Ctx) error {
	idString := ctx.Request.PathValue("id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		return dto.NewError("INVALID_URL_PARAM", http.StatusBadRequest, "invalid car id", err)
	}

	var req schemas.UpdateCar

	err = ctx.ParseJSON(&req)
	if err != nil {
		return err
	}

	car, err := c.car.Update(ctx.Context(), car.UpdateCarParams{
		CarID: id,
		Mark:  req.Mark,
		Model: req.Model,
		Year:  req.Year,
	})
	if err != nil {
		return err
	}

	return ctx.WriteJSON(car, http.StatusOK)
}
