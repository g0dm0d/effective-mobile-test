package models

type Car struct {
	ID       int
	RegNum   string
	Mark     string
	Model    string
	Year     uint16
	OwnderID int
}
