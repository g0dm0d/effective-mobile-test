package models

type Person struct {
	ID         int
	Name       string
	Surname    string
	Patronymic string
}
