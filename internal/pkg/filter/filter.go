package filter

import (
	"net/url"
	"reflect"
)

func ParseQueryParams(queryParams url.Values, params interface{}) {
	value := reflect.ValueOf(params).Elem()
	valueType := value.Type()

	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)
		tag := valueType.Field(i).Tag.Get("param")
		if tag != "" {
			if queryParams.Has(tag) {
				field.Set(reflect.ValueOf(queryParams.Get(tag)))
			}
		}
	}
}
