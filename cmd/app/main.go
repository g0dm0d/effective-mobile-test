package main

import (
	"log"
	"log/slog"
	"os"

	"gitlab.com/g0dm0d/effective/internal/config"
	"gitlab.com/g0dm0d/effective/internal/gateway/carapi"
	"gitlab.com/g0dm0d/effective/internal/server"
	"gitlab.com/g0dm0d/effective/internal/server/handler"
	"gitlab.com/g0dm0d/effective/internal/service"
	"gitlab.com/g0dm0d/effective/internal/service/car"
	"gitlab.com/g0dm0d/effective/internal/store/postgres"
)

func main() {
	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))
	config := config.Load()

	db, err := postgres.New(config.Postgres.DSN, config.Postgres.Migration)
	if err != nil {
		logger.Error("Error opening postgres connection", "error", err)
		os.Exit(1)
	}

	peopleStore := postgres.NewPeopleStore(db)
	carStore := postgres.NewCarStore(db)

	carAPIClient := carapi.NewClient()
	carService := car.NewService(carStore, peopleStore, carAPIClient)

	services := service.Services{
		Car: carService,
	}

	carHandler := handler.NewHandlerCar(services.Car)

	handlers := handler.Handlers{
		Car: carHandler,
	}

	server, err := server.NewServer(config.App.Addr, config.App.Port, handlers, logger)
	if err != nil {
		logger.Error("Error creating server", "error", err)
		os.Exit(1)
	}

	err = server.RunServer()
	if err != nil {
		log.Panic(err)
	}
}
